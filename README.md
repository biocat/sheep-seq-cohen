
## Sheep Seq Cohen project

This is the code based used to analyze data associated with the manuscript “Extrauterine support by EXTEND allows premature lamb brains to achieve similar transcriptomic profiling to late pre-term lambs” by Jennifer L. Cohen et al. Our group has developed an extra-uterine environment for newborn development (EXTEND) using an ovine model, that aims to mimic the womb in an attempt to improve short and long-term health outcomes associated with prematurity. The objective of this study was to determine the histologic and transcriptomic consequences of EXTEND on the brain.
 

All raw RNA-Seq files and associated phenotypic data are available from the
Gene Expression Omnibus database [GSE275228](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE275228).
User will need to update directory to working folder e.g. ``` setwd("<set/path/to/working/directory/>") ```. 


The markdown starts from processed RNA-Seq count data. The GSEA code chunk is computationally taxing and time consuming.  As such, output variables are provided in the 'import' folder. To rerun these loops, or customize with desired parameters, go to the relevant section and remove ``` "eval=FALSE" ``` from the markdown section header. An example .html file produced from the rMarkdown file is provided.


The following libraries will be needed:

* library(readxl)
* library(limma)
* library(edgeR)
* library(tableone)
* library(Homo.sapiens)
* library(ggplot2)
* library(ggpubr)
* library(gridExtra)
* library(RColorBrewer)
* library(kableExtra)
* library(DT)
* library(ggrepel)
* library(AnnotationHub)
* library(ggVennDiagram)
* library(viridis)
* library(ComplexHeatmap)
* library(clusterProfiler)
* library(scales)
* library(ComplexUpset)
* library(enrichplot)





